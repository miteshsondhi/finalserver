import java.awt.BorderLayout;
import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.xml.ws.handler.MessageContext;

public class Server extends JFrame {
	private JTextField userText;
	private JScrollPane chatWindow;
	private JTextArea chat;
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private ServerSocket server;
	private Socket connection;
	private JTextField port;
	private JButton startServer;
	private JPanel panel;
	private int portValue;
	private JTextField lable;
	private Thread thread;
	private String message = "";

	// constructor
	public Server() {
		panel = new JPanel();
		panel.setLayout(null);
		port = new JTextField("8888");

		// start server button
		startServer = new JButton("Start Server");
		startServer.setSize(125, 25);
		startServer.setLocation(25, 5);
		panel.add(startServer);
		startServer.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				thread = new Thread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						try {
							startConnection();
							setStreams();
							recieveData();

						} catch (ClassNotFoundException | IOException e) {

						}
					}
				});
				thread.start();

			}

			// function for open a port
			private void startConnection() throws IOException,
					ClassNotFoundException {
				portValue = Integer.parseInt(port.getText());
				server = new ServerSocket(portValue, 100);
				connection = server.accept();

			}

			// do nothing
			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}
		});

		// port
		port = new JTextField("8888");
		port.setSize(100, 25);
		port.setLocation(200, 5);
		panel.add(port);

		// chat window
		chat = new JTextArea();
		chat.setSize(300, 150);
		chat.setLocation(25, 75);
		chatWindow = new JScrollPane(chat);
		chatWindow.setSize(300, 150);
		chatWindow.setLocation(25, 75);
		panel.add(chatWindow);
		// user text
		userText = new JTextField();
		userText.setSize(300, 25);
		userText.setLocation(25, 235);
		panel.add(userText);
		userText.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					if (!connection.isClosed()) {
						sendData();
					} else {
						showMessage("Please check your connection");
					}
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			private void sendData() throws IOException {
				if (!connection.isClosed()) {
					output.writeObject(userText.getText());
					output.flush();
					showMessage("Server :" + userText.getText());
					if (userText.getText().equals("EXIT")) {
						message = "EXIT";
					}
					userText.setText("");

				} else {
					userText.setText("");
					showMessage("Please check your connection");
				}
			}
		});

		// panel
		this.add(panel);
		this.setSize(350, 300);
		this.setVisible(true);
	}

	public void showMessage(final String string) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				chat.append("\n" + string);

			}
		});
	}

	public void recieveData() throws ClassNotFoundException, IOException {
		while (!message.equals("EXIT")) {
			try {
				message = (String) input.readObject();
				showMessage("Client : " + message);

			} catch (IOException e) {

			}

		}
		closeConnections();
	}

	public void setStreams() throws IOException {
		output = new ObjectOutputStream(connection.getOutputStream());
		output.flush();
		input = new ObjectInputStream(connection.getInputStream());
		showMessage("Connection is established");
	}

	public void closeConnections() throws IOException {
		input.close();
		output.close();
		connection.close();
		server.close();
		message = "";
	}
}